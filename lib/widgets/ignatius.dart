import 'package:flutter/material.dart';

class Ignatius extends StatelessWidget {
  const Ignatius({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 185.0,
      child: Image.asset(
        'images/ignatius.png',
        fit: BoxFit.scaleDown,
      ),
    );
  }
}
