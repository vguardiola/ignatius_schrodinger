import 'package:flutter/material.dart';
import 'screens/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ignatius Schrodinger',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Material(
        color: Colors.white70,
        child: Home(),
      ),
    );
  }
}
