import 'dart:math';

import 'package:ignatius_schrodinger/widgets/ignatius.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  HomeState createState() => HomeState();
}

class HomeState extends State<Home> with TickerProviderStateMixin {
  Animation<double> ignatiusAnimation;
  Animation<double> flapsAnimation;
  AnimationController ignatiusAnimationController;
  AnimationController flapsAnimationController;
  AnimationController boxAnimationController;
  static AudioCache player = new AudioCache();

  Animation<double> boxAnimation;

  @override
  void initState() {
    super.initState();
    ignatiusAnimationController = new AnimationController(vsync: this, duration: Duration(seconds: 2));
    flapsAnimationController = new AnimationController(vsync: this, duration: Duration(seconds: 1));
    boxAnimationController = new AnimationController(vsync: this, duration: Duration(seconds: 1));
    ignatiusAnimation = Tween(begin: 0.0, end: -170.0).animate(
      CurvedAnimation(
        parent: ignatiusAnimationController,
        curve: Curves.linear,
      ),
    );

    boxAnimation = Tween(begin: -0.005, end: 0.005).animate(
      CurvedAnimation(
        parent: boxAnimationController,
        curve: Curves.linear,
      ),
    );

    flapsAnimation = Tween(begin: 0.0, end: -0.7).animate(
      CurvedAnimation(
        parent: flapsAnimationController,
        curve: Curves.easeIn,
      ),
    );
    ignatiusAnimation.addStatusListener((status) {
      if (ignatiusAnimation.status == AnimationStatus.dismissed) {
        flapsAnimationController.reverse();
        boxAnimationController.forward();
      }
    });
    boxAnimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        boxAnimationController.reverse();
      }
      if (status == AnimationStatus.dismissed) {
        boxAnimationController.forward();
      }
    });
    boxAnimationController.forward();
    player.fetchToMemory('../images/grito.mp3');
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          buildIgnatiusAnimation(),
          GestureDetector(
            onTap: () {
              if (ignatiusAnimationController.status == AnimationStatus.dismissed) {
                playLocal();
                boxAnimationController.stop();
                ignatiusAnimationController.forward();
                flapsAnimationController.forward();
              } else {
                ignatiusAnimationController.reverse();
              }
            },
            child: buildBox(),
          ),
          buildLeftFlap(),
          buildRightFlap(),
        ],
      ),
    );
  }

  void playLocal() async {
    player.play('../images/grito.mp3');
  }

  Widget buildIgnatiusAnimation() {
    return AnimatedBuilder(
      builder: (context, child) {
        return Positioned(
          child: child,
          top: ignatiusAnimation.value,
          right: 0,
          left: 0,
        );
      },
      animation: ignatiusAnimation,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Ignatius(),
      ),
    );
  }

  Widget buildBox() {
    return AnimatedBuilder(
      builder: (context, child) {
        return Transform.rotate(
          angle: pi * boxAnimation.value,
          child: child,
        );
      },
      animation: boxAnimation,
      child: Container(
        width: 200.0,
        height: 200.0,
        color: Colors.brown,
        // margin: EdgeInsets.only(top: 130),
        child: Align(
          child: Text(
            'Push Me!',
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),
          ),
          alignment: AlignmentDirectional.center,
        ),
      ),
    );
  }

  Widget buildLeftFlap() {
    return Positioned(
      left: 5,
      top: 5,
      child: AnimatedBuilder(
        builder: (context, child) {
          return Transform.rotate(
            child: child,
            angle: pi * flapsAnimation.value,
            alignment: Alignment.topLeft,
          );
        },
        child: Container(
          width: 100,
          height: 5,
          color: Colors.brown,
          // transform: Transform,
        ),
        animation: flapsAnimation,
      ),
    );
  }

  Widget buildRightFlap() {
    return Positioned(
      right: 2,
      top: 5,
      child: AnimatedBuilder(
        builder: (context, child) {
          return Transform.rotate(
            child: child,
            angle: -pi * flapsAnimation.value,
            alignment: Alignment.topRight,
          );
        },
        child: Container(
          width: 100,
          height: 5,
          color: Colors.brown,
          // transform: Transform,
        ),
        animation: flapsAnimation,
      ),
    );
  }
}
